﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication8
{

    public class ConvertTime
    {

        private string perYearStr, perMonthStr, perDayOfMonthStr;
        private int perYear, perMonth, perDayOfMonth;
        private int year, month, day;

        public ConvertTime()
        {
            DateTime time = DateTime.Now;
            this.year = time.Year;
            this.month = time.Month;
            this.day = time.Day;
            get();
        }

        public ConvertTime (int year, int month, int day)
        {
            this.year = year;
            this.month = month;
            this.day = month;
            get();
        }

        private void get ()
        {
            DateTime time = DateTime.Now;

            PersianCalendar pc = new PersianCalendar();

            DateTime dt = new DateTime(year, month, day); // یا DateTime.Now

            perYear = pc.GetYear(dt);
            perMonth = pc.GetMonth(dt);
            perDayOfMonth = pc.GetDayOfMonth(dt);

            perYearStr = perYear.ToString();
            perMonthStr = ((perMonth < 10) ? "0" + perMonth : perMonth.ToString());
            perDayOfMonthStr = ((perDayOfMonth < 10) ? "0" + perDayOfMonth : perDayOfMonth.ToString());

        }

        public string getPerYearStr ()
        {
            return perYearStr;
        }


        public string getPerMonthStr ()
        {
            return perMonthStr;
        }


        public string getPerDayOfMonthStr ()
        {
            return perDayOfMonthStr;
        }


        public int getPerYear ()
        {
            return perYear;
        }


        public int getPerMonth ()
        {
            return perMonth;
        }


        public int getPerDayOfMonth ()
        {
            return perDayOfMonth;
        }

    }
}
