﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication8
{
    public partial class Form1 : Form
    {

        private int lenM = 12;
        private int lenD = 31;

        public Form1()
        {
            InitializeComponent();

            for (int i = 1; i <= lenM; i++) comboMonth.Items.Add(i.ToString());
            for (int i = 1; i <= lenD; i++) comboDay.Items.Add(i.ToString());
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string yearTxt = txtYear.Text;
            int year;
            if (int.TryParse (yearTxt, out year))
            {
                int month = Int32.Parse (comboMonth.SelectedItem.ToString ());
                int day = Int32.Parse (comboDay.SelectedItem.ToString ());

                ConvertTime convertTime = new ConvertTime (year , month , day);
 
                lblResult.Text = getResultConvert (convertTime);

            }
            else MessageBox.Show("Error");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConvertTime convertTime = new ConvertTime ();

            lblResult.Text = getResultConvert (convertTime);
        }

        private string getResultConvert (ConvertTime convertTime)
        {
            return string.Format("Result: {0}/{1}/{2}",
                convertTime.getPerYearStr(),
                convertTime.getPerMonthStr(),
                convertTime.getPerDayOfMonthStr());
        }

    }
}
